import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailComponent } from './content/detail/detail.component';
import { DashboardComponent } from './content/dashboard/dashboard.component';


const routes: Routes = [
  {
  path: 'weather' + '/:city',
  component: DetailComponent,
  pathMatch: 'full',
},
  {
      path: '**',
      component: DashboardComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
