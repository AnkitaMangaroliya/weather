
export class WeatherModel {
    name: string;
    sunrise: number;
    sunset: number;
    temp: number;

    constructor(data) {
        this.name = data.name || '';
        this.sunrise = data.sunrise || undefined;
        this.sunset = data.sunset || undefined;
        this.temp = data.temp || undefined;
    }
}