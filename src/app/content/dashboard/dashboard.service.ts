import { Injectable } from '@angular/core';

import { HttpService } from 'src/core/http.service';
import { GlobalConstants } from 'src/core/global-constant';

import { HttpClient } from '@angular/common/http';

import { WeatherModel } from './dashboard.model';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http: HttpClient, private httpService: HttpService) {
  }

  getCurrentWeather(cityName) {
    const apiPath = `${GlobalConstants.ApiPath.GetCurrentWeather}?q=${cityName}&appid=${GlobalConstants.Appid}`;
    return this.httpService.get<WeatherModel>(apiPath);
  }
  
}
