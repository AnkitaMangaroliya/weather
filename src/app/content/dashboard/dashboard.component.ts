import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { DashboardService } from './dashboard.service';
import { WeatherModel } from './dashboard.model';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  cityName= ['London', 'Paris', 'Amsterdam', 'Lisbon', 'Dublin'];
  weatherCollection: WeatherModel[] = [];
  getDetailSubscribe: Subscription[] = [];

  constructor(
    private dashboardService: DashboardService
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  ngOnDestroy() {
    if (
      this.getDetailSubscribe !== undefined &&
      this.getDetailSubscribe.length > 0
    ) {
      this.getDetailSubscribe.forEach((data, index) => {
        this.getDetailSubscribe[index].unsubscribe();
      });
    }
  }

  getData() {
    for (let i=0; i< this.cityName.length; i++){
      this.getWeatherData(this.cityName[i], i);
    }
  }

  getWeatherData(cityName, index) {
    if (this.getDetailSubscribe !== undefined && index !== null && this.getDetailSubscribe[index]) {
      this.getDetailSubscribe[index].unsubscribe();
    }

    this.getDetailSubscribe[index] = this.dashboardService.getCurrentWeather(cityName)
      .subscribe((response: any) => {
        this.weatherCollection.push({
          name: response.name,
          sunrise: response.sys.sunrise,
          sunset: response.sys.sunset,
          temp: response.main.temp          
        });
        this.getDetailSubscribe[index].unsubscribe();
      }, (err) => {
        this.getDetailSubscribe[index].unsubscribe();
      });
  }
}
