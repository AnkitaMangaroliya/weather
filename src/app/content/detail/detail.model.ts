
export class WeatherDetailModel {
    date: string;
    seaLevel: number;
    temp: number;

    constructor(data) {
        this.date = data.date || '';
        this.seaLevel = data.seaLevel || undefined;
        this.temp = data.temp || undefined;
    }
}