import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

import { DetailService} from './detail.service';
import { WeatherDetailModel } from './detail.model';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  cityName = '';
  weatherData: any;
  time = "09:00:00";
  weatherSummaryData: WeatherDetailModel[] = [];
  getDetailSubscribe: Subscription;

  constructor(private activateRoute: ActivatedRoute,
      private detailService: DetailService) {}

  ngOnInit(): void {
    
    this.activateRoute.params.subscribe(params => {
      if (params['city']) {
        this.cityName = params['city'];
        this.getDetail();
      }
    });
  }

  ngOnDestroy() {

    if (this.getDetailSubscribe !== undefined) {
        this.getDetailSubscribe.unsubscribe();
    }
  }
  
  getDetail() {

    if (this.getDetailSubscribe !== undefined) {
      this.getDetailSubscribe.unsubscribe();
  }

  this.getDetailSubscribe = this.detailService.gettWeatherDetail(this.cityName)
      .subscribe((response: any) => {
        this.weatherData = response;
        this.getSpecificTimeData();
      }, (err) => {
      });
  }


  getSpecificTimeData() {
    if (this.weatherData && this.weatherData.list && this.weatherData.list.length) {
      this.weatherData.list.forEach( item => {
        if (item.dt_txt) {
          const time = item.dt_txt.split(' ');
          if (time && time.length && time[1] === this.time) {
            this.weatherSummaryData.push({
              date: item.dt_txt,
              seaLevel: item.main.sea_level,
              temp: item.main.temp
            });
          }
        }
      });
    }
  }

}
