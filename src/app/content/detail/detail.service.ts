import { Injectable } from '@angular/core';

import { HttpService } from 'src/core/http.service';
import { GlobalConstants } from 'src/core/global-constant';

import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DetailService {

  constructor(private http: HttpClient, private httpService: HttpService) {
  }

  gettWeatherDetail(cityName) {
    const apiPath = `${GlobalConstants.ApiPath.GetForecast}?q=${cityName}&appid=${GlobalConstants.Appid}`;
    return this.httpService.get<any>(apiPath);
  }
  
}
