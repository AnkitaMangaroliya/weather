import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from "@angular/common/http";

import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DetailComponent } from './content/detail/detail.component';
import { DashboardComponent } from './content/dashboard/dashboard.component';
import { ToolbarComponent } from './toolbar/toolbar/toolbar.component';
import { DashboardService } from './content/dashboard/dashboard.service';
import { HttpService } from 'src/core/http.service';
import { ContentComponent } from './content/content.component';


@NgModule({
  declarations: [
    AppComponent,
    DetailComponent,
    DashboardComponent,
    ToolbarComponent,
    ContentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatCardModule,
    FlexLayoutModule,
    MatIconModule,
    MatSidenavModule,
    MatToolbarModule,
  ],
  providers: [DashboardService, HttpService],
  bootstrap: [AppComponent]
})
export class AppModule { }
