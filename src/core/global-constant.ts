export const GlobalConstants = {
    
    Appid: '3d8b309701a13f65b660fa2c64cdc517',
    ApiPath: {
        GetCurrentWeather: 'data/2.5/weather',
        GetForecast: 'data/2.5/forecast',
    },
    Host: 'http://api.openweathermap.org/'
}

