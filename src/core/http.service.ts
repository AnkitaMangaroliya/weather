import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from "@angular/common/http";
import { Observable, throwError as observableThrowError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { ErrorModel } from './common.model';
import { GlobalConstants } from './global-constant';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class HttpService {
    constructor(
        private http: HttpClient,
        private router: Router,
    ) {

    }

    
    get<TReturn>(apiPath: string, searchParams?: any): Observable<any> {
        let headers = new HttpHeaders({});
        return this.http.get(GlobalConstants.Host + (apiPath), { headers }).pipe(
            map(this.extractData),
            catchError(this.handleError));
    }


    private extractData(res: Response): any {
        if (res === null) {
            return [];
        } else {
            const body: any = res;
            return body || {};
        }
    }

    private handleError(error: any) {
        // console.warn(error);
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        // checking if usr is logged in or not. If not then do not redirect. if yes then redirect

        if (error.status === 401) {
            return observableThrowError(error.error);
        }
        if (error.status === 400 && error.hasOwnProperty('error')) {
            return observableThrowError(error.error);
        } else if (error.status === 503) {
            const errorData: ErrorModel = { ErrorCode: 503, ErrorMessage: 'Service Not Available' };
            return observableThrowError(errorData);
        } else if (error.status === 0) {
            const errorData: ErrorModel = { ErrorCode: 0, ErrorMessage: 'Internet Not Connect' };
            return observableThrowError(errorData);
        }
        return observableThrowError(JSON.parse(error._body));
    }
}
