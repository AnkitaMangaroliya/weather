

export class ErrorModel {
    ErrorCode: number;
    ErrorMessage: string;

    constructor(data) {
        this.ErrorCode = data.ErrorCode || -1;
        this.ErrorMessage = data.ErrorMessage || '';
    }
}